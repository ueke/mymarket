package com.codevilla.myMarket.modules.models;

import java.io.Serializable;

public class Loginmodel implements Serializable {


	private String email;
	private String password;
	private Long shopid;
	
	public Long getShopid() {
		return shopid;
	}
	public void setShopid(Long shopid) {
		this.shopid = shopid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
