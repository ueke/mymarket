package com.codevilla.myMarket.modules.models;

import java.io.Serializable;

public class StaffModel implements Serializable{

    private Long id;
    private String name;
    private String email;
    private String password;
    private String isStaffActive;   // 1-true, 0-false
    private String role;
    private String phone;
    private String Address;
    private String passwordChangeRequired;  // 1-true, 0-false
    private String image; 
    private String shopname;
    private Long shopid;
    private Long ownersid; // Optional field
    
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getPasswordChangeRequired() {
		return passwordChangeRequired;
	}
	public void setPasswordChangeRequired(String passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIsStaffActive() {
		return isStaffActive;
	}
	public void setIsStaffActive(String isStaffActive) {
		this.isStaffActive = isStaffActive;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public Long getOwnersid() {
		return ownersid;
	}
	public void setOwnersid(Long ownersid) {
		this.ownersid = ownersid;
	}
	public Long getShopid() {
		return shopid;
	}
	public void setShopid(Long shopid) {
		this.shopid = shopid;
	}
	
    
    
}