package com.codevilla.myMarket.modules.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.codevilla.myMarket.modules.entity.Expenses;


@Repository
@Transactional
public interface ExpensesRepository extends JpaRepository<Expenses, Long> {

}
