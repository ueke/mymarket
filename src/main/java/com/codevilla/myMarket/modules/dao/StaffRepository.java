package com.codevilla.myMarket.modules.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.codevilla.myMarket.modules.entity.Staff;
import com.codevilla.myMarket.modules.entity.Tenant;



@Repository
@Transactional
public interface StaffRepository extends JpaRepository<Staff, Long> {

	Staff findByEmailAndShopid(String email, Long shopid);
	
	

    @Modifying
    @Query("update Staff U set U.password = :password, U.passwordChangeRequired = :passwordChangeRequired where U.email = :email")
    int updatePassword(@Param("password") String password,
    		@Param("email") String email,
    		@Param("passwordChangeRequired") String passwordChangeRequired);
    
    

    @Modifying
    @Query("update Staff U set U.imagefilepath = :imagefilepath where U.id = :id and U.shopid = :shopid ")
    int updateStaffImageRecord(@Param("imagefilepath") String imagefilepath ,@Param("id") Long id,@Param("shopid") Long shopid);
	
    
    @Modifying
    @Query("select Staff U where U.id = :id and U.shopid = :shopid")
    Staff getStaffByID(@Param("id") Long staffid, @Param("shopid") Long shopid);
    
	

	/*
    @Modifying
    @Query("update Staff U set U.role = :role, U.email=:email, U.phone = :phone, U.dept=:dept, U.staffIsActive=:staffIsActive where U.id = :id")
    int updateStaffRecord(@Param("role") String role, @Param("email") String email, @Param("phone") String phone, 
    		@Param("dept") String dept,@Param("staffIsActive") String staffIsActive,@Param("id") Long id);


    @Modifying
    @Query("select Staff U where U.id = :id")
    Staff getStaffByID(@Param("id") int id);
    
    @Modifying
    @Query("update Staff U set U.passwordChangeRequired = :passwordChangeRequired, U.password=:password where U.email = :email")
    int updatePasswordChangeRequired(@Param("passwordChangeRequired") String passwordChangeRequired,
    		@Param("email") String email,@Param("password") String password);
	
    @Modifying
    @Query("update Staff U set U.password = :password, U.passwordChangeRequired = :passwordChangeRequired where U.email = :email")
    int updatePassword(@Param("password") String password,
    		@Param("email") String email,
    		@Param("passwordChangeRequired") String passwordChangeRequired);

*/
	
}