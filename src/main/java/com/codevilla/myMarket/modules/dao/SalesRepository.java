package com.codevilla.myMarket.modules.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.codevilla.myMarket.modules.entity.Sales;


@Repository
@Transactional
public interface SalesRepository extends JpaRepository<Sales, Long> {

}
