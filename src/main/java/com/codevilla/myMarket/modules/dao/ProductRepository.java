package com.codevilla.myMarket.modules.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.codevilla.myMarket.modules.entity.Product;


@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	Optional<Product> findById(Long id);
	
	List<Product> findProductByShopid(long shopid);
	
	
    @Modifying
    @Query("update Product U set U.price = :price, U.dateupdated=:dateupdated where U.id = :id and U.shopid = :shopid")
    int updateProductPrice(@Param("price") String price, @Param("dateupdated") Date dateupdated,@Param("id") Long id,@Param("shopid") Long shopid);

    
    @Modifying
    @Query("update Product U set U.productname = :productname, U.productdescription=:productdescription, U.price = :price, U.dateupdated=:dateupdated where U.id = :id and U.shopid = :shopid")
    int updateProduct(@Param("productname") String productname, @Param("productdescription") String productdescription, @Param("price") String price, 
    		@Param("dateupdated") Date dateupdated,@Param("id") Long id,@Param("shopid") Long shopid);

	
	
	
}
