package com.codevilla.myMarket.modules.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.codevilla.myMarket.modules.entity.Tenant;


@Repository
@Transactional
public interface TenantRepository extends JpaRepository<Tenant, Long> {
	
	
    @Modifying
    @Query("update Tenant U set U.logofilepath = :logofilepath where U.id = :id")
    int updateTenantLogoRecord(@Param("logofilepath") String logofilepath,@Param("id") Long id);

    @Modifying
    @Query("select Tenant U where U.id = :id")
    Tenant getTenantByID(@Param("id") Long shopid);
    
	
	//CREATE SCHEMA FOR NEW SHOP
  //  @Query("CREATE SCHEMA IF NOT EXISTS :schemaname")
   // Optional<Integer> createSchema(@Param("schemaname") String schemaname);
    

}
