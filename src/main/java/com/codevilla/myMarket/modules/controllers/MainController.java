package com.codevilla.myMarket.modules.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.codevilla.myMarket.modules.dao.ActivitiesLogRepository;
import com.codevilla.myMarket.modules.dao.ExpensesRepository;
import com.codevilla.myMarket.modules.dao.ProductRepository;
import com.codevilla.myMarket.modules.dao.SalesRepository;
import com.codevilla.myMarket.modules.dao.StaffRepository;
import com.codevilla.myMarket.modules.dao.TenantRepository;
import com.codevilla.myMarket.modules.entity.ActivitiesLog;
import com.codevilla.myMarket.modules.entity.Expenses;
import com.codevilla.myMarket.modules.entity.Product;
import com.codevilla.myMarket.modules.entity.Sales;
import com.codevilla.myMarket.modules.entity.Staff;
import com.codevilla.myMarket.modules.entity.Tenant;
import com.codevilla.myMarket.modules.models.ChangePasswordModel;
import com.codevilla.myMarket.modules.models.Loginmodel;
import com.codevilla.myMarket.modules.models.Response;
import com.codevilla.myMarket.modules.models.SalesModel;
import com.codevilla.myMarket.modules.models.StaffModel;
import com.codevilla.myMarket.modules.services.DataService;
import com.coronationam.AssetMgtMiddleware.controllerModule.ApiResponse;
import com.coronationam.AssetMgtMiddleware.model.ImageStore;


@CrossOrigin(origins = "http://localhost:50035" , maxAge = 3600)
@RestController
public class MainController {
	
	Logger logger = LoggerFactory.getLogger(MainController.class);
	
	
	@Autowired
    private StaffRepository staffRepository;
	

	@Autowired
    private ProductRepository productRepository;
	
	@Autowired
    private ExpensesRepository expensesRepository;
	
	@Autowired
    private SalesRepository salesRepository;
	
	@Autowired
    private TenantRepository tenantRepository;
	

	@Autowired
    private ActivitiesLogRepository activitiesLogRepository;
	

	@Autowired
    private DataService service;
	

	//Register staff
    @RequestMapping(value = "/registerShop", method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Object registerShop(@RequestBody Tenant req, @RequestParam("file") MultipartFile file) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {
    		
    		Tenant shop = new Tenant();
    		
        	shop.setShopname(req.getShopname());
        	shop.setShopaddress(req.getShopaddress());
        	shop.setShoplogo(req.getShoplogo());
        	
        	shop.setCountry(req.getCountry());
        	shop.setDatecreated(new Date());
        	
        	shop.setOwneremail(req.getOwneremail());
        	shop.setOwnername(req.getOwnername());
        	shop.setOwnerphone(req.getOwnerphone());
        	
        	Tenant res = tenantRepository.save(shop);
        	
        	if (res!=null) {
        		
        		
        		Staff model = new Staff();
                model.setAddress(req.getShopaddress());
                model.setEmail(req.getOwneremail());
            	
              
                model.setIsStaffActive("1");  // staff account is activate
                model.setName(req.getOwnername());
                model.setPhone(req.getOwnerphone());
                model.setRole("Manager");
                model.setPasswordChangeRequired("1"); // 1-true, 0-false
                model.setPassword("XXetbaf@*vs");
                model.setImage(null);
                model.setShopname(req.getShopname());
                model.setShopid(res.getId());
            
                Staff resp = staffRepository.save(model);
            
            	if (resp!=null) {
            		
                    response.setStatus("00");
                    response.setData(null);
                    response.setMessage("Account created Successfully");
                    
          //        Staff staff = staffRepository.findUserByEmail(req.getEmail());
                    
                    /*
                    CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
    						 "Added new staff: "+req.getName());
                    */

                    try {
                    
                    service.sendEmailVisitor(req.getOwneremail(),"myMarketDotcom","You have successfully creates an account on myMarket. Below are your account details"+"\n"+" Customer ID: "+req.getOwneremail()+"\n"
                            +"Default password: XXetbaf@*vs");

                    } catch (Exception ex) {
                        
                    	response.setStatus("01");
                    	response.setData(ex.getMessage());
                    	response.setMessage("Your account creation might have been successful, please contact I.T for help"); // change this to - An error occur, please try again
                    	
                    	logger.info("Account creation failed, when sending mail: "+ex.getMessage());
                    }
	
            	}
        		
        		
        	}
    		
        	
    		
    		} catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account creation failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account creation failed, when sending mail: "+ex.getMessage());
      
        }
        return response;
       
    }
    
    
    
	
	//Register staff
    @RequestMapping(value = "/RegisterStaffAccount", method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Object RegisterStaffAccount(@RequestBody StaffModel req,@RequestParam("file") MultipartFile file) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

            Staff staffDetail = staffRepository.findByEmailAndShopid(req.getEmail(),req.getShopid());

            if (staffDetail != null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("Staff Email already exist");
            	
            } else {
            
            	Staff model = new Staff();
                model.setAddress(req.getAddress());
                model.setEmail(req.getEmail());
            	
                String isActive;
               	if(req.getIsStaffActive() == null) {
                		isActive = "1";
                	}else {
                		isActive = req.getIsStaffActive();
                	}
              
                model.setIsStaffActive(isActive);  // staff account is activate
                model.setName(req.getName());
                model.setPhone(req.getPhone());
                model.setRole(req.getRole());
                model.setPasswordChangeRequired("1"); // 1-true, 0-false
                model.setPassword("XXetbaf@*vs");
                model.setImage(req.getImage());
                model.setShopname(req.getShopname());
                model.setShopid(req.getShopid());
            
                Staff resp = staffRepository.save(model);
                
            	if (resp!=null) {
            		
                    response.setStatus("00");
                    response.setData(null);
                    response.setMessage("Account created Successfully");
                    
                  //  Staff staff = staffRepository.findUserByEmail(req.getEmail());
                    
                    /*
                    CreateActivityLog(staff.getName(),staff.getPhone(),staff.getEmail(),
    						 "Added new staff: "+req.getName());
                    */

                    try {
                    
                    service.sendEmailVisitor(req.getEmail(),"Asset Management","Your account was created successfully. Below are your account details"+"\n"+" Customer ID: "+req.getEmail()+"\n"
                            +"Default password: XXetbaf@*vs");

                    } catch (Exception ex) {
                        
                    	response.setStatus("01");
                    	response.setData(ex.getMessage());
                    	response.setMessage("Your account creation might have been successful, please contact I.T for help"); // change this to - An error occur, please try again
                    	
                    	logger.info("Account creation failed, when sending mail: "+ex.getMessage());
                    }
	
            	}

            }
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account creation failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account creation failed, when sending mail: "+ex.getMessage());
      
        }
        return response;
       
    }
    
    
   
	//Login 
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Object login(@RequestBody Loginmodel req) {
        
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Staff staffDetail = staffRepository.findByEmailAndShopid(req.getEmail(),req.getShopid());

    		
            if (staffDetail == null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("User does not exist");
            	
     }else if((req.getPassword()).toLowerCase().equals(("XXetbaf@*vs").toLowerCase()) && staffDetail.getPasswordChangeRequired().equals("0")){
            		
    	response.setStatus("01");
    	response.setData(null);
    	response.setMessage("Incorrect password");
            		
     }
    else if((req.getPassword()).toLowerCase().equals(("XXetbaf@*vs").toLowerCase()) && staffDetail.getPasswordChangeRequired().equals("1")){
		
    		response.setStatus("02");
    		response.setData(staffDetail);
    		response.setMessage("Change Default password");
		
    }
    else if((req.getPassword()).equals(decryptPassword(staffDetail.getPassword()))){
		
        response.setStatus("00");
        response.setData(staffDetail);
        response.setMessage("success");
    
        
    //    CreateActivityLog(staffDetail,"Logged in and view");
        
        
   }else {
	   
	   response.setStatus("01");
   	   response.setData(null);
   	   response.setMessage("Incorrect Username and password");
	   
   	}
            	
   } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("An error occur"); // change this to - An error occur, please try again 	
        	logger.info("login failed : "+ex.getMessage());
      
        }
    	
        return response;
       
    }
    
    // Forgot Password
	

    //forgot password method
    @RequestMapping(value = "/forgotPassword/{email}/{shopid}", method = RequestMethod.POST)
    public Object forgotPassword(@PathVariable("email") String email, @PathVariable("shopid") Long shopid) {

 	Response<Object> response = new Response<Object>();
    	
    	try {
    

    		Staff staffDetail = staffRepository.findByEmailAndShopid(email,shopid);

            if (staffDetail == null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("User does not exist");
            	
            } else {
            	
           // 	staffRepository.updatePasswordChangeRequired("1", email,"XXetbaf@*vs");
            	 response.setStatus("00");
                 response.setData(staffDetail);
                 response.setMessage("Your password reset link was successful, check your mail for the reset password");
                
                 try {
                     
                	 String link = "https://www.google.com/webhp?hl=en&sa=X&ved=0ahUKEwi9pJ_FgZvtAhUDYcAKHX_1CjwQPAgI";
                	 
                     service.sendEmailVisitor(email,"myMarket"," Click this link to reset your password:  "+link);

                     } catch (Exception ex) {
                         
                     	response.setStatus("01");
                     	response.setData(ex.getMessage());
                     	response.setMessage("Your password reset was successful, check your mail for the reset password"); // change this to - An error occur, please try again
                     
                     	logger.info("password set failed, when sending mail: "+ex.getMessage());
                     }
                 
                 
            }
        
    	} catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("An error occur"); // change this to - An error occur, please try again 	
        	logger.info("password reset failed : "+ex.getMessage());
      
        }

    	
    	
    	return response;
    }
    
    // Change Password
    


	//Change password
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public Object changePassword(@RequestBody ChangePasswordModel req) {
    	
    	Response<Object> response = new Response<Object>();
    	
    	try {

    		Staff staffDetail = staffRepository.findByEmailAndShopid(req.getEmail(), req.getShopid());

            if (staffDetail==null) {
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("This user does not exist");
            	
            }
            
            else if(req.getCurrentpassword().equals("XXetbaf@*vs") && staffDetail.getPasswordChangeRequired().equals("1")) {
            	
            	  staffRepository.updatePassword(encryptPassword(req.getNewpassword()), req.getEmail(), "0");
                  response.setStatus("00");
                  response.setData(staffDetail);
                  response.setMessage("Account updated Successfully");
            }
            
            
            else if(!((req.getCurrentpassword()).equals(decryptPassword(staffDetail.getPassword())))) {
            	
            	response.setStatus("01");
            	response.setData(null);
            	response.setMessage("incorrect password");
            }
            
            else {
                staffRepository.updatePassword(encryptPassword(req.getNewpassword()), req.getEmail(),"0");
                response.setStatus("00");
                response.setData(null);
                response.setMessage("Account updated Successfully");

            }
        } catch (Exception ex) {
          	response.setStatus("11");
        	response.setData(ex.getMessage());
        	response.setMessage("Account update failed"); // change this to - An error occur, please try again
        	
        	logger.info("Account update failed: "+ex.getMessage());
      
        }
        return response;
       
    }
	 
    // Activity Log
    /*
    @Async
    private void CreateActivityLog(Staff staff, String actionPerformed) {
    	    	
    	ActivitiesLog log =new ActivitiesLog();
    	
    	log.setAction_performed(actionPerformed);
    	log.setEmail(staff.getEmail());
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        log.setLoggedin_date(formatter.format(date));
        log.setLoggedin_user(staff.getName());
        log.setPhone(staff.getPhone());
        log.setShop_ownersid(staff.getOwnersid());
        log.setShopid(staff.getShopid());
        log.setShopname(staff.getShopname());
        
        activitiesLogRepository.save(log);
        
    } 
*/   
    
    // PRODUCT
    
    //Add product
    
    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    public Object addProduct(@RequestBody Product product) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	Product resp = productRepository.save(product);
    	if (resp!=null) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("Product added successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to add product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to add product : "+ex.getMessage());
    	}
    	return response;
    }
    

    @RequestMapping(value = "/saveSales", method = RequestMethod.POST)
    public Object saveSales(@RequestBody Sales sales) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	Sales resp = salesRepository.save(sales);
    	if (resp!=null) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("Sales added successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to add product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to add product : "+ex.getMessage());
    	}
    	return response;
    }
    
    
    @RequestMapping(value = "/saveExpenses", method = RequestMethod.POST)
    public Object saveSales(@RequestBody Expenses expenses) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	Expenses resp = expensesRepository.save(expenses);
    	if (resp!=null) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("Expenses added successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to add product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to add product : "+ex.getMessage());
    	}
    	return response;
    }
    
    

    @RequestMapping(value = "/saveListOfSales", method = RequestMethod.POST)
    public Object saveListOfSales(@RequestBody List<Sales> sales) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	List<Sales> resp = salesRepository.saveAll(sales);
    	if (resp!=null) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("List of sales added successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to add product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to add product : "+ex.getMessage());
    	}
    	return response;
    }
    
    
    @RequestMapping(value = "/saveListOfExpenses", method = RequestMethod.POST)
    public Object saveListOfExpenses(@RequestBody List<Expenses> expenses) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	List<Expenses> resp = expensesRepository.saveAll(expenses);
    	if (resp!=null) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("Expenses added successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to add product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to add product : "+ex.getMessage());
    	}
    	return response;
    }
    
    
    
    
    //get all product
    @GetMapping("/getAllProduct/{shopid}")
    public Object getAllProduct(@PathVariable("shopid") Long shopid) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	List<Product> resp = productRepository.findProductByShopid(shopid);
    	
    	if (resp.size()>0) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to get product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to get product : "+ex.getMessage());
    	}
    	return response;
    }
    
    
  //get product
    @GetMapping("/getProduct/{productid}")
    public Object getProduct(@PathVariable("productid") Long productid) {
    	
    	Response<Object> response = new Response<Object>();
    	try {
    		
    	Optional<Product> resp = productRepository.findById(productid);
    	
    	if (resp.isPresent()) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("successfully");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("Failed to get product");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("Failed to get product : "+ex.getMessage());
    	}
    	return response;
    }
    
    
    
    
    // update product details
    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    public Object updateProduct(@RequestBody Product product) {
    	

    	Response<Object> response = new Response<Object>();
    	try {
    		
    	int resp = productRepository.updateProduct(product.getProductname(), product.getProductdescription(),
    											product.getPrice(), product.getDateupdated(),product.getId(),product.getShopid());
    	
    	if (resp>0) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("update successful");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("update failed");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("update failed : "+ex.getMessage());
    	}
    	return response;
    	
    }
    
    // update price of product
    @RequestMapping(value = "/updateProductPrice/{productid}/{new_price}", method = RequestMethod.POST)
    public Object updateProductPrice(@PathVariable("productid") Long productid,@PathVariable("shopid") Long shopid,
    									@PathVariable("new_price") String new_price) {
    	

    	Response<Object> response = new Response<Object>();
    	try {
    	
    	int resp = productRepository.updateProductPrice(new_price,new Date(),productid,shopid);
    	
    	
    	if (resp>0) {
    		
    		response.setStatus("00");
        	response.setData(resp);
        	response.setMessage("update successful");
    	}{
    		response.setStatus("01");
        	response.setData(null);
        	response.setMessage("update failed");
    	}
    	
    	}
    	catch(Exception ex) {
    		
    		response.setStatus("02");
        	response.setData(null);
        	response.setMessage(ex.getMessage());
        	
        	logger.info("update failed : "+ex.getMessage());
    	}
    	return response;
    	
    }
    
    
    
    
    // UPLOADED IMAGES
    
    public Response<Object> profilePixUploads(@PathVariable("shopid") Long shopid, @PathVariable("staffid") Long staffid,
		 @RequestParam("files") MultipartFile files) throws Exception {
	 Response<Object> response = new Response<Object>();
	 if (files != null) {
		 try {
			 String realPathtoUploads = System.getenv("SystemDrive") + "\\UPLOAD"+shopid;

			 //   String realPathtoUploads =  System.getenv("SystemDrive")+"\\Users\\Uyi.eke\\CoronationProjects\\image";

			 if (!new File(realPathtoUploads).exists()) {
				 new File(realPathtoUploads).mkdir();
			 }

			 String imageName = shopid + "_" + staffid + "_" + files.getOriginalFilename();

			 String filePath = realPathtoUploads + "\\" + imageName;
			 Path path = Paths.get(filePath);
			 boolean imagePathExist = false;
			 File file = new File(realPathtoUploads);
			 File[] imageFile = file.listFiles();
			 for (File f : imageFile) {
				 if (f.getName().contains(shopid + "_" + staffid)) {
					 f.delete();
					 imagePathExist = true;
				 }
			 }

			 Files.write(path, files.getBytes());


			 int val;
			 if (staffid.equals("0")) {
				 val = tenantRepository.updateTenantLogoRecord(filePath,shopid);
			 } else {
				 val = staffRepository.updateStaffImageRecord(filePath,staffid,shopid);
			 }

			 if(val == 0) {
				 response.setMessage("Uploads failed");
		 			response.setStatus("11");
		 			response.setData("");
			 }else {
				 

				 response.setMessage("Uploads successful");
				 response.setStatus("00");
				 response.setData("");
				 
			 }

		 } catch (Exception ex) {
			 response.setMessage("Uploads failed");
			 response.setStatus("11");
			 response.setData("");
		 }

	 		} else {
	 			response.setMessage("Uploads failed");
	 			response.setStatus("11");
	 			response.setData("");
	 		}
	 return response;
    	}

    
    
    @GetMapping("/profilePictureDownload/{custID}")
    public byte[] downloadImageDownload(@PathVariable("staffid") Long staffid,@PathVariable("shopid") Long shopid) {

    	String imagePath = null;
    	
    	if(staffid==0) {
    		Tenant tenant= tenantRepository.getTenantByID(shopid);
    		imagePath = tenant.getLogofilepath();
    		
    	}else {
    		Staff staff= staffRepository.getStaffByID(staffid, shopid);
    		imagePath = tenant.getLogofilepath();
    	}
    	
        ImageStore imageObj = imageStoreRepository.getImage(custID);
        byte[] imageByte = null;

        if (imageObj != null) {
            File file = new File(imageObj.getPath());
            try {
                imageByte = Files.readAllBytes(Paths.get(imageObj.getPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imageByte;
    } 
    
    
/*
    @RequestMapping(value = "/profilePixUpload/{appID}/{custID}", method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Response<Object> profilePixUploads(@PathVariable("appID") String appID, @PathVariable("custID") String custID,
                                                 @RequestParam("files") MultipartFile files) throws Exception {
    	Response<Object> response = new Response<Object>();
        if (files != null) {
            try {
                String realPathtoUploads = System.getenv("SystemDrive") + "\\myMarketUPLOAD";

                if (!new File(realPathtoUploads).exists()) {
                    new File(realPathtoUploads).mkdir();
                }
                String imageName = custID + "_" + files.getOriginalFilename();
                String filePath = realPathtoUploads + "\\" + imageName;
                Path path = Paths.get(filePath);
                boolean imagePathExist = false;
                File file = new File(realPathtoUploads);
                File[] imageFile = file.listFiles();
                for (File f : imageFile) {
                    if (f.getName().contains(custID)) {
                        f.delete();
                        imagePathExist = true;
                    }
                }

                Files.write(path, files.getBytes());

                ImageStore imageStore = new ImageStore();
                imageStore.setCustid(custID);
                imageStore.setPath(filePath);

                if (imagePathExist) {
                    imageStoreRepository.updateImage(custID, filePath);
                } else {
                    imageStoreRepository.save(imageStore);
                }

                response.setMessage("Uploads successful");
                response.setStatus("00");
                response.setData("");

            } catch (Exception ex) {
                response.setMessage("Uploads failed");
                response.setStatus("11");
                response.setData("");
            }

        } else {
            response.setMessage("Uploads failed");
            response.setStatus("11");
            response.setData("");
        }
        return response;
    }

    @GetMapping("/profilePictureDownload/{custID}")
    public byte[] downloadPixDownload(@PathVariable("custID") String custID) {

        ImageStore imageObj = imageStoreRepository.getImage(custID);
        byte[] imageByte = null;

        if (imageObj != null) {
            File file = new File(imageObj.getPath());
            try {
                imageByte = Files.readAllBytes(Paths.get(imageObj.getPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imageByte;
    }
    
    */
    
    
    public static String encryptPassword(String value) {
 	   String key = "JavasEncryptDemo"; // 128 bit key
        String randomVector = "RandomJavaVector"; // 16 bytes IV
 	
 	 try {
          IvParameterSpec iv = new IvParameterSpec(randomVector.getBytes("UTF-8"));
          SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
          Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
          cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
          byte[] encrypted = cipher.doFinal(value.getBytes());
         // System.out.println("encrypted text: "  + Base64.encodeBase64String(encrypted));
          return Base64.encodeBase64String(encrypted);
      } catch (Exception e) {
          e.printStackTrace();
      }
      return "UEpIrpddkzxrzS+Q3z/sWg==";

 }
 
    public static String decryptPassword(String encrypted) {
 	
 	   String key = "JavasEncryptDemo"; // 128 bit key
        String randomVector = "RandomJavaVector"; // 16 bytes IV
 	
 	  try {
           IvParameterSpec iv = new IvParameterSpec(randomVector.getBytes("UTF-8"));
           SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
           Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
           cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
           byte[] originalText = cipher.doFinal(Base64.decodeBase64(encrypted));
          // System.out.println("decrypted text: "  + new String(originalText));
           return new String(originalText);
       } catch (Exception e) {
           e.printStackTrace();
       }
       return "Password@111";
  
  
 }



    
}
