package com.codevilla.myMarket.modules.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sales")
public class Sales {

    @Id
    @GeneratedValue
    private Long id;
    
    private Date date;
    private Long shopid;
    private Long productid;
    private String amount;
    
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "staffid", referencedColumnName = "id")
    private Staff staff;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Long getShopid() {
		return shopid;
	}


	public void setShopid(Long shopid) {
		this.shopid = shopid;
	}


	public Long getProductid() {
		return productid;
	}


	public void setProductid(Long productid) {
		this.productid = productid;
	}


	public String getAmount() {
		return amount;
	}


	public void setAmount(String amount) {
		this.amount = amount;
	}


	public Staff getStaff() {
		return staff;
	}


	public void setStaff(Staff staff) {
		this.staff = staff;
	}
   
   
    
    
    
}
    