package com.codevilla.myMarket.modules.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "activity_log")
public class ActivitiesLog {


    @Id
    @GeneratedValue
    private Long id;
    
    private String shopname;
    private Long shopid;
  
    private String loggedin_userid;
    private String loggedin_username;
    private String loggedin_date;
    private String phone;
    private String email;
    private String action_performed;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public Long getShopid() {
		return shopid;
	}
	public void setShopid(Long shopid) {
		this.shopid = shopid;
	}
	public String getLoggedin_userid() {
		return loggedin_userid;
	}
	public void setLoggedin_userid(String loggedin_userid) {
		this.loggedin_userid = loggedin_userid;
	}
	public String getLoggedin_username() {
		return loggedin_username;
	}
	public void setLoggedin_username(String loggedin_username) {
		this.loggedin_username = loggedin_username;
	}
	public String getLoggedin_date() {
		return loggedin_date;
	}
	public void setLoggedin_date(String loggedin_date) {
		this.loggedin_date = loggedin_date;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAction_performed() {
		return action_performed;
	}
	public void setAction_performed(String action_performed) {
		this.action_performed = action_performed;
	}
    
  
	
}
