package com.codevilla.myMarket.modules.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "staff")
public class Staff {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String email;
    private String password;
    private String isStaffActive;   // 1-true, 0-false
    private String role;
    private String phone;
    private String Address;
    private String passwordChangeRequired;  // 1-true, 0-false
    private String imagefilepath; 
    private String shopname;
    
    private Long shopid;
    
    /*
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "shopid", referencedColumnName = "id")
    private Tenant tenant;
    */
    
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getPasswordChangeRequired() {
		return passwordChangeRequired;
	}
	public void setPasswordChangeRequired(String passwordChangeRequired) {
		this.passwordChangeRequired = passwordChangeRequired;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIsStaffActive() {
		return isStaffActive;
	}
	public void setIsStaffActive(String isStaffActive) {
		this.isStaffActive = isStaffActive;
	}
	
	public String getImagefilepath() {
		return imagefilepath;
	}
	public void setImagefilepath(String imagefilepath) {
		this.imagefilepath = imagefilepath;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public Long getShopid() {
		return shopid;
	}
	public void setShopid(Long shopid) {
		this.shopid = shopid;
	}

    
    
}