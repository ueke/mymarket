package com.codevilla.myMarket.modules.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "tenant")
public class Tenant {

/* Add shop to shopDatabase
   Create schema for shop
   create database tables inside the created schema

*/

	@Id
    @GeneratedValue
    private Long id;
	private Date datecreated;
    private String country;
    
    private String shopname;
    private String shopaddress;
    private String logofilepath;
    
    private String ownername;
    private String owneremail;
    private String ownerphone;
    

    
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopname() {
		return shopname;
	}
	public void setShopname(String shopname) {
		this.shopname = shopname;
	}
	public String getShopaddress() {
		return shopaddress;
	}
	public void setShopaddress(String shopaddress) {
		this.shopaddress = shopaddress;
	}
	
	
	
	public String getLogofilepath() {
		return logofilepath;
	}
	public void setLogofilepath(String logofilepath) {
		this.logofilepath = logofilepath;
	}
	public String getOwnername() {
		return ownername;
	}
	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}
	public String getOwneremail() {
		return owneremail;
	}
	public void setOwneremail(String owneremail) {
		this.owneremail = owneremail;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public Date getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(Date datecreated) {
		this.datecreated = datecreated;
	}
	public String getOwnerphone() {
		return ownerphone;
	}
	public void setOwnerphone(String ownerphone) {
		this.ownerphone = ownerphone;
	}
    
	
	
	
	
    public boolean isValid() {
        return shopname != null && shopaddress != null && country != null && owneremail != null
        		&& ownerphone != null && ownername != null ;
    }
    
    
    /*
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ownersid", referencedColumnName = "id")
    private Staff staff;
    */
    
    
    
    
	
}
