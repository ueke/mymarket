package com.codevilla.myMarket.modules.services;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


@Component
public class DataService {

 private static final Logger logger = LoggerFactory.getLogger(DataService.class);


 @Autowired
 JavaMailSender sender;

 public void sendEmailVisitor(String toEmail, String subject, String body) throws Exception {
     //    EmailBody emailBody = emailUtilRepository.getEmailBodyByName(Constants.THANK_YOU);
     //  String body = emailBody.getBody().replace("var1", visitorName);
     MimeMessage message = sender.createMimeMessage();
     MimeMessageHelper helper = new MimeMessageHelper(message, true);
     helper.setFrom(Constants.sendingEmail);
     helper.setTo(toEmail);
     helper.setText(body, true);
     helper.setSubject(subject);
     sender.send(message);
 }

	
}
